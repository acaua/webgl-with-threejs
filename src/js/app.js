console.time('Initialize');
import App from './app/index';
import batchInstantiate from './modules/batch-instantiate';
import supportsWebGL from './modules/supports-webgl';
import Navbar from './components/navbar';
import ThreeBP from './components/three-boilerplate';
import Intro from './components/intro';
import Ex1 from './components/example-1';
import Ex2 from './components/example-2';
import Ex3 from './components/example-3';
import Ex4 from './components/example-4';
import Ex5 from './components/example-5';
import Ex6 from './components/example-6';
import Ex7 from './components/example-7';

const app = new App(),
	$canvasContainer = $('#canvas');

let examples = [Ex1, Ex2, Ex3, Ex4, Ex5, Ex6, Ex7],
	instance;

function hashHandler() {
	let hash = window.location.hash;

	if (instance instanceof ThreeBP) {
		instance.destroy();
	} else {
		$canvasContainer.empty();
	}

	if (!hash.length) {
		instance = new Intro('#canvas');
		instance.init();
		return;
	}

	instance = new examples[parseInt(hash.substr(1)) - 1]('#canvas');
	instance.init();
}

app.init(function () {
	console.log('%cMantis Starter', 'color: #338656; font: 50px sans-serif;');
	console.debug(this);

	batchInstantiate('.navbar', Navbar);

	if (supportsWebGL()) {
		window.onhashchange = hashHandler;
		hashHandler();
	}

	console.timeEnd('Initialize');
});
