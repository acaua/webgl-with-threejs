export default class ThreeBP {
	constructor(selector) {
		this.selector = selector;
		this.width = window.innerWidth;
		this.height = window.innerHeight;
		this.frameEvent = new Event('frame');
	}

	setScene() {
		this.scene = new THREE.Scene();
		this.scenary = new THREE.Object3D;

		this.scene.add(this.scenary);
	}

	setCamera() {
		this.camera = new THREE.PerspectiveCamera(50, this.width/this.height, 1, 20000);
		this.camera.position.y = 25;
		this.camera.position.z = 300;
	}

	setRenderer() {
		this.renderer = new THREE.WebGLRenderer({
			antialias: true
		});
		this.renderer.setSize(this.width, this.height);
		this.canvas = document.querySelector(this.selector).appendChild(this.renderer.domElement);
	}

	setControls() {
		this.controls = new THREE.OrbitControls(this.camera, this.canvas);
	}

	addHelpers() {
		this.axes = new THREE.AxesHelper(500);
		this.scenary.add(this.axes);
	}

	addLights() {
		this.ambientLight = new THREE.AmbientLight(0x555555);
		this.directionalLight = new THREE.DirectionalLight(0xffffff);
		this.directionalLight.position.set(10, 0, 10).normalize();

		this.scenary.add(this.ambientLight);
		this.scenary.add(this.directionalLight);
	}

	render() {
		this.renderer.render(this.scene, this.camera);
		this.canvas.dispatchEvent(this.frameEvent);
		this.frameRequest = window.requestAnimationFrame(this.render.bind(this));
	}

	destroy() {
		window.cancelAnimationFrame(this.frameRequest);
		this.scene.children = [];
		this.canvas.remove();
	}

	init() {
		this.setScene();
		this.setCamera();
		this.setRenderer();
		this.setControls();
		this.addLights();
		this.render();
	}
}
