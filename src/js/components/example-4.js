import ThreeBP from './three-boilerplate';

export default class Example extends ThreeBP{
	constructor(selector) {
		super(selector);
		this.textureLoader = new THREE.TextureLoader();
	}

	setControls() {
		super.setControls();
		this.controls.maxDistance = 500;
		this.controls.minDistance = 200;
	}

	addSky() {
		let radius = 400,
			segments = 50;

		this.skyGeometry = new THREE.SphereGeometry(radius, segments, segments);
		this.skyMaterial = new THREE.MeshPhongMaterial({
			color: 0x666666,
			side: THREE.BackSide,
			shininess: 0
		});
		this.sky = new THREE.Mesh(this.skyGeometry, this.skyMaterial);

		this.scenary.add(this.sky);

		this.loadSkyTextures();
	}

	loadSkyTextures() {
		this.textureLoader.load('./img/textures/earth/sky-texture.jpg', texture => {
			this.skyMaterial.map = texture;
			this.skyMaterial.needsUpdate = true;
		});
	}

	addEarth() {
		let radius = 100,
			segments = 50;

		this.earthGeometry = new THREE.SphereGeometry(radius, segments, segments);
		this.earthMaterial = new THREE.ShaderMaterial({
			bumpScale: 5,
			specular: new THREE.Color(0x333333),
			shininess: 50,
			uniforms: {
				sunDirection: {
					value: new THREE.Vector3(1, 1, .5)
				},
				dayTexture: {
					value: this.textureLoader.load('./img/textures/earth/earth-texture.jpg')
				},
				nightTexture: {
					value: this.textureLoader.load('./img/textures/earth/earth-night.jpg')
				}
			},
			vertexShader: this.dayNightShader.vertex,
			fragmentShader: this.dayNightShader.fragment
		});
		this.earth = new THREE.Mesh(this.earthGeometry, this.earthMaterial);

		this.scenary.add(this.earth);

		this.loadEarthTextures();
		this.addAtmosphere();
	}

	loadEarthTextures() {
		this.textureLoader.load('./img/textures/earth/earth-texture.jpg', texture => {
			this.earthMaterial.map = texture;
			this.earthMaterial.needsUpdate = true;
		});
		this.textureLoader.load('./img/textures/earth/earth-bump.jpg', texture => {
			this.earthMaterial.bumpMap = texture;
			this.earthMaterial.needsUpdate = true;
		});
		this.textureLoader.load('./img/textures/earth/earth-specular.jpg', texture => {
			this.earthMaterial.specularMap = texture;
			this.earthMaterial.needsUpdate = true;
		});
	}

	addAtmosphere() {
		this.innerAtmosphereGeometry = this.earthGeometry.clone();
		this.innerAtmosphereMaterial = THREEx.createAtmosphereMaterial();
		this.innerAtmosphereMaterial.uniforms.glowColor.value.set(0x88ffff);
		this.innerAtmosphereMaterial.uniforms.coeficient.value = 1;
		this.innerAtmosphereMaterial.uniforms.power.value = 5;
		this.innerAtmosphere = new THREE.Mesh(this.innerAtmosphereGeometry, this.innerAtmosphereMaterial);
		this.innerAtmosphere.scale.multiplyScalar(1.008);

		this.outerAtmosphereGeometry = this.earthGeometry.clone();
		this.outerAtmosphereMaterial = THREEx.createAtmosphereMaterial();
		this.outerAtmosphereMaterial.side = THREE.BackSide;
		this.outerAtmosphereMaterial.uniforms.glowColor.value.set(0x0088ff);
		this.outerAtmosphereMaterial.uniforms.coeficient.value = .68;
		this.outerAtmosphereMaterial.uniforms.power.value = 10;
		this.outerAtmosphere = new THREE.Mesh(this.outerAtmosphereGeometry, this.outerAtmosphereMaterial);
		this.outerAtmosphere.scale.multiplyScalar(1.06);

		this.earth.add(this.innerAtmosphere);
		this.earth.add(this.outerAtmosphere);
	}

	get dayNightShader() {
		return {
			vertex: `
				varying vec2 vUv;
				varying vec3 vNormal;

				void main() {
					vUv = uv;
					vec4 mvPosition = modelViewMatrix * vec4(position, 1.0);
					vNormal = normalMatrix * normal;
					gl_Position = projectionMatrix * mvPosition;
				}
			`,
			fragment: `
				uniform sampler2D dayTexture;
				uniform sampler2D nightTexture;

				uniform vec3 sunDirection;

				varying vec2 vUv;
				varying vec3 vNormal;

				void main(void) {
					vec3 dayColor = texture2D(dayTexture, vUv).rgb;
					vec3 nightColor = texture2D(nightTexture, vUv).rgb;

					float cosineAngleSunToNormal = dot(normalize(vNormal), sunDirection);

					cosineAngleSunToNormal = clamp(cosineAngleSunToNormal * 5.0, -1.0, 1.0);

					float mixAmount = cosineAngleSunToNormal * 0.5 + 0.5;

					vec3 color = mix(nightColor, dayColor, mixAmount);

					gl_FragColor = vec4(color, 1.0);
				}
			`
		}
	}

	animate() {
		this.canvas.addEventListener('frame', () => {
			this.scenary.rotation.x += 0.0001;
			this.scenary.rotation.y -= 0.0005;
		});
	}

	init() {
		super.init();
		this.addSky();
		this.addEarth();
		this.animate();
	}
}
