import ThreeBP from './three-boilerplate';

export default class Example extends ThreeBP{
	constructor(selector) {
		super(selector);
		this.textureLoader = new THREE.TextureLoader();
		this.STLLoader = new THREE.STLLoader();
	}

	addMesh() {
		let material = new THREE.MeshPhongMaterial({
			color: 0x111111,
			specular: 0x888888,
			shininess: 10
		});

		this.STLLoader.load('./models/lion.stl', geometry => {
			this.mesh = new THREE.Mesh(geometry, material);
			this.mesh.scale.set(5, -5, 5);
			this.scenary.add(this.mesh);
		});
	}

	animate() {
		this.canvas.addEventListener('frame', () => {
			this.mesh.rotation.y -= .001;
		});
	}

	init() {
		super.init();
		this.addMesh();
		this.animate();
	}
}
