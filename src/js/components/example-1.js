export default class Example {
	constructor(selector) {
		let canvas = document.createElement('canvas');

		this.canvas = document.querySelector(selector).appendChild(canvas);
		this.canvas.width = window.innerWidth;
		this.canvas.height = window.innerHeight;
		this.context = this.canvas.getContext('2d');
	}

	addSquare() {
		this.context.fillRect(200, 100, 300, 300);
	}

	init() {
		this.addSquare();
	}
}
