export default class Example {
	constructor(selector) {
		this.canvas = document.querySelector(selector).appendChild(document.createElement('canvas'));
		this.canvas.width = window.innerWidth;
		this.canvas.height = window.innerHeight;
		this.context = this.canvas.getContext("webgl");
		this.context.viewport(0, 0, this.canvas.width, this.canvas.height);
		this.aspect = this.canvas.width / this.canvas.height;
	}

	setVertexShader(shader) {
		this.vertexShader = this.context.createShader(this.context.VERTEX_SHADER);
		this.context.shaderSource(this.vertexShader, `
			attribute vec2 position;

			void main() {
				gl_Position = vec4(position, 0.0, 1.0);
			}
		`);
		this.context.compileShader(this.vertexShader);
	}

	setFragmentShader(shader) {
		this.fragmentShader = this.context.createShader(this.context.FRAGMENT_SHADER);
		this.context.shaderSource(this.fragmentShader, `
			#ifdef GL_ES
			precision highp float;
			#endif

			uniform vec4 color;

			void main() {
				gl_FragColor = color;
			}
		`);
		this.context.compileShader(this.fragmentShader);
	}

	setProgram() {
		this.program = this.context.createProgram();
		this.context.attachShader(this.program, this.vertexShader);
		this.context.attachShader(this.program, this.fragmentShader);
		this.context.linkProgram(this.program);
		this.program.color = this.context.getUniformLocation(this.program, 'color');
		this.program.position = this.context.getAttribLocation(this.program, 'position');

	}

	createBuffer(vertices) {
		let buffer = this.context.createBuffer();
		this.context.bindBuffer(this.context.ARRAY_BUFFER, buffer);
		this.context.bufferData(this.context.ARRAY_BUFFER, vertices, this.context.STATIC_DRAW);
	}

	addSquare() {
		let vertices = new Float32Array([
			-0.5, 0.5 * this.aspect,
			0.5, 0.5 * this.aspect,
			0.5, -0.5 * this.aspect,
			-0.5, 0.5 * this.aspect,
			0.5, -0.5 * this.aspect,
			-0.5, -0.5 * this.aspect
		]),
		itemSize = 2,
		numItems = vertices.length / itemSize;

		this.createBuffer(vertices);

		this.context.useProgram(this.program);
		this.context.uniform4fv(this.program.color, [0.0, 0.0, 0.0, 1.0]);

		this.context.enableVertexAttribArray(this.program.position);
		this.context.vertexAttribPointer(this.program.position, itemSize, this.context.FLOAT, false, 0, 0);

		this.context.drawArrays(this.context.TRIANGLES, 0, numItems);
	}

	init() {
		this.setVertexShader();
		this.setFragmentShader();
		this.setProgram();
		this.addSquare();
	}
}
