import ThreeBP from './three-boilerplate';
import {transition} from '../modules/transition';

export default class Example extends ThreeBP{
	addMesh() {
		this.geometry = new THREE.BoxGeometry(200, 100, 100);
		this.material = new THREE.MeshPhongMaterial({
			color: 0xffffff
		});
		this.mesh = new THREE.Mesh(this.geometry, this.material);

		this.scenary.add(this.mesh);
	}

	animate() {
		transition(0, Math.PI * 2.25, value => {
			this.mesh.rotation.y = value;
		}, 2000);
	}

	init() {
		super.init();
		this.addMesh();
		this.animate();
	}
}
