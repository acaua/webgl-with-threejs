import ThreeBP from './three-boilerplate';

export default class Intro extends ThreeBP {
	setLightColor() {
		this.ambientLight.color.setHex(0x88ffff);
		this.directionalLight.color.setHex(0xff3333);
	}

	addFog() {
		this.fog = new THREE.Fog(0xff0055, 300, 800);
		this.scene.fog = this.fog;
	}

	addGround() {
		this.ground = new THREE.GridHelper(800, 80, 0xff00ff, 0xff00ff);
		this.scenary.add(this.ground);
	}

	addLogo() {
		this.logo = new THREE.Object3D;
		this.scenary.add(this.logo);

		this.addText();
	}

	addText() {
		let loader = new THREE.FontLoader(),
			material = new THREE.MeshStandardMaterial({
				color: 0xdddddd,
				roughness: .5,
				metalness: .7
			});

		loader.load('fonts/robot-crush.json', font => {
			let geometry = new THREE.TextGeometry('WebGL with', {
				font: font,
				size: 30,
				height: 8
			});
			geometry.center();

			this.text1 = new THREE.Mesh(geometry, material);
			this.text1.position.y = 50;

			this.logo.add(this.text1);
		});

		loader.load('fonts/brushstrike.json', font => {
			let geometry = new THREE.TextGeometry('three.js', {
				font: font,
				size: 20,
				height: 3
			});
			geometry.center();

			this.text2 = new THREE.Mesh(geometry, material);
			this.text2.position.x = 70;
			this.text2.position.y = 20;
			this.text2.rotation.z = 0.2;

			this.logo.add(this.text2);
		});
	}

	addEffects() {
		this.composer = new THREE.EffectComposer(this.renderer);
		this.composer.addPass(new THREE.RenderPass(this.scene, this.camera));

		this.RGBShift = new THREE.ShaderPass(THREE.RGBShiftShader);
		this.RGBShift.uniforms.amount.value = 0.001;
		this.composer.addPass(this.RGBShift);

		this.glitchPass = new THREE.GlitchPass();
		this.glitchPass.renderToScreen = true;
		this.composer.addPass(this.glitchPass);

	}

	animate() {
		let ground = {
				step: .5,
				limit: 10
			},
			logo = {
				positionTime: 800,
				positionAmplitude: 8,
				positionInitial: 10,
				rotationTime: 1000,
				rotationAmplitude: 0.4
			},
			now;

		this.canvas.addEventListener('frame', () => {
			now = Date.now();

			this.composer.render();

			this.ground.position.z += ground.step;
			if (this.ground.position.z === ground.limit) {
				this.ground.position.z = 0;
			}

			this.logo.position.y = (Math.sin(now / logo.positionTime) * logo.positionAmplitude) + logo.positionInitial;
			this.logo.rotation.y = Math.sin(now / logo.rotationTime) * logo.rotationAmplitude;
		});
	}

	init() {
		super.init();

		this.setLightColor();
		this.addFog();
		this.addGround();
		this.addLogo();
		this.addEffects();

		this.animate();
	}
}
