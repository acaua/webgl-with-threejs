import ThreeBP from './three-boilerplate';

export default class Example extends ThreeBP{
	constructor(selector) {
		super(selector);

		this.rows = 20;
		this.columns = 20;
		this.spacing = 1;
		this.meshSize = 10;
		this.meshes = [];

		this.meshGeometry = new THREE.BoxGeometry(this.meshSize, 1, this.meshSize);
	}

	addMeshes() {
		for (let c = 0; c < this.columns; c++) {
			for (let r = 0; r < this.rows; r++) {
				let mesh = this.createMesh(
					c * (this.meshSize + this.spacing),
					0,
					r * (this.meshSize + this.spacing)
				);

				if (r === 0) {
					this.meshes[c] = [];
				}

				this.meshes[c][r] = mesh;
				this.scenary.add(mesh);
			}
		}
	}

	createMesh(x, y, z) {
		let material = new THREE.MeshLambertMaterial(),
			mesh = new THREE.Mesh(this.meshGeometry, material);

		mesh.position.set(
			-((this.meshSize + this.spacing) * this.columns) / 2,
			0,
			-((this.meshSize + this.spacing) * this.rows) / 2
		).add(new THREE.Vector3(x, y, z));
		return mesh;
	}

	animate() {
		let now,
			speed,
			amplitude = 6;

		this.canvas.addEventListener('frame', () => {
			now = Date.now(),
			speed = now / 200;

			this.scenary.rotation.x = (Math.sin(now / 1000) * .2) + .4;
			this.scenary.rotation.y = Math.sin(now / 1000) * .8;

			for (let c = 0; c < this.columns; c++) {
				for (let r = 0; r < this.rows; r++) {
					let mesh = this.meshes[c][r],
						height = ((Math.sin(c + speed)) * (Math.PI * amplitude)) / 6 + (((Math.sin(r + speed)) * (Math.PI * amplitude)) / 6);

					mesh.position.y = height;
					mesh.scale.y = - amplitude - height;
					mesh.material.color.setRGB((height / 10) + .8, (height / 15) + .1, 0);
				}
			}
		});
	}

	init() {
		super.init();
		this.addMeshes();
		this.animate();
	}
}
