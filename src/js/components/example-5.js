import ThreeBP from './three-boilerplate';

export default class Example extends ThreeBP{
	constructor(selector) {
		super(selector);
		this.globeRadius = 100;
		this.particlesCount = 500;
		this.particlesDistance = 20;
	}

	addGlobe() {
		let segments = 40;

		this.globeGeometry = new THREE.SphereGeometry(this.globeRadius, segments, segments);
		this.globeMaterial = new THREE.MeshPhongMaterial({
			color: 0x222222,
			wireframe: true,
			visible: false
		});
		this.globe = new THREE.Mesh(this.globeGeometry, this.globeMaterial);

		this.scenary.add(this.globe);
	}

	addParticles() {
		let radius = .5,
			segments = 10;

		this.particles = [];
		this.particleGeometry = new THREE.SphereGeometry(radius, segments, segments);
		this.particleMaterial =  new THREE.MeshBasicMaterial({
			color: 0xff0044
		});
		this.lineMaterial = new THREE.LineBasicMaterial({
			color: 0xff0044
		});

		for (let i = 0; i < this.particlesCount; i++) {
			let particle = this.createParticle(
					this.getRandomPosition('lat'),
					this.getRandomPosition('lon'),
					this.globeRadius
				);

			this.particles.push(particle);
			this.globe.add(particle);

			this.particles.forEach(p => {
				if (particle !== p) {
					if (particle.position.distanceTo(p.position) < this.particlesDistance) {
						this.globe.add(this.createLine(particle.position, p.position));
					}
				}
			});
		}
	}

	createParticle(lat, lon, radius){
		let particle = new THREE.Mesh(this.particleGeometry, this.particleMaterial),
			position = this.latLonToVector3(lat, lon, radius);

		particle.position.set(position.x, position.y, position.z);
		particle.velocity = Math.random();

		return particle;
	}

	createLine(positionStart, positionEnd) {
		let geometry = new THREE.Geometry();

		geometry.vertices.push(new THREE.Vector3(
			positionStart.x,
			positionStart.y,
			positionStart.z,
		));
		geometry.vertices.push(new THREE.Vector3(
			positionEnd.x,
			positionEnd.y,
			positionEnd.z,
		));

		return new THREE.Line(geometry, this.lineMaterial);
	}

	getRandomPosition(type) {
		let max = (type === 'lat') ? 80 : 180;
		return (Math.random() * (max * 2)) - max;
	}

	latLonToVector3(lat, lon, radius, heigth = 0) {
		let phi = lat * Math.PI / 180,
			theta = (lon - 180) * Math.PI / 180,
			x = -(radius + heigth) * Math.cos(phi) * Math.cos(theta),
			y = (radius + heigth) * Math.sin(phi),
			z = (radius + heigth) * Math.cos(phi) * Math.sin(theta);
		return new THREE.Vector3(x, y, z);
	}

	animate() {
		this.canvas.addEventListener('frame', () => {
			this.globe.rotation.y += .005;
			this.globe.rotation.z += .002;
		});
	}

	init() {
		super.init();
		this.addGlobe();
		this.addParticles();
		this.animate();
	}
}
