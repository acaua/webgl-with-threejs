import ThreeBP from './three-boilerplate';

export default class Example extends ThreeBP{
	addMesh() {
		this.geometry = new THREE.BoxGeometry(200, 100, 100);
		this.material = new THREE.MeshPhongMaterial({
			color: 0xffffff
		});
		this.mesh = new THREE.Mesh(this.geometry, this.material);

		this.scenary.add(this.mesh);
	}

	animate() {
		this.canvas.addEventListener('frame', () => {
			this.mesh.rotation.x += 0.005;
			this.mesh.rotation.y += 0.002;
		});
	}

	init() {
		super.init();
		this.addMesh();
		this.animate();
		window.tt = this;
	}
}
