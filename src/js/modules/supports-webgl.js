export default function () {
	var canvas = document.createElement('canvas');

	try {
		return !!(
			window.WebGLRenderingContext &&
			(
				canvas.getContext('webgl') ||
				canvas.getContext('experimental-webgl')
			)
		);
	} catch (e) {
		return false;
	}
}
