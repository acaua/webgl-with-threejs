function percentage(num, total, decimal) {
	return parseFloat(((num  * 100) / total).toFixed(decimal || 3));
}

export function transition(from, to, callback, duration, easing) {
	let diff = to - from,
		startTime = Date.now(),
		elapsed;

	function loop() {
		elapsed = Date.now() - startTime;

		callback(((diff / 100) * percentage(elapsed, duration)) + from);

		if (elapsed >= duration) {
			callback(to);
			return;
		}

		window.requestAnimationFrame(loop);
	};

	loop();
}
